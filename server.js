var express = require('express');
var mysql = require('mysql');
var app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
//npm run start:server

// require("../nodelearing/src/app/routes/customer.routes")(app);


app.use(cors());
// Configuring body parser middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'password',
    database: 'hotels',
    port: 3306
});

connection.connect(function (err) {
    if (err) {
        console.log('Error connecting to Db');
        console.log(err);
        connection.release();
        // return;
    }
    console.log('Connection established');
});



app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept',
    );
    next();
});

//show all products
app.get('/getHotel', (req, res) => {
    let sql = 'SELECT * FROM hotels.hoteldetails where tag = "hotels"';
    let query = connection.query(sql, (err, results) => {
        if (err) throw err;
        // res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        res.send(results);
    });
});

app.get('/getVillas&Appartment', (req, res) => {
    let sql = 'SELECT * FROM hotels.hoteldetails where tag = "villas" OR tag = "Appartment";';
    let query = connection.query(sql, (err, results) => {
        if (err) throw err;
        // res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        res.send(results);
    });
});

app.get('/getCountry', (req, res) => {
    let sql = 'SELECT name FROM hotels.country';
    let query = connection.query(sql, (err, results) => {
        if (err) throw err;
        // res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
        res.send(results);
    });
});

app.post('/addProduct', (req, res) => {
    console.log(req.body);
    var user_id = req.body.user_id;
    var hotelTag = req.body.hotelTag;
    var hotelName = req.body.hotelName;
    var hotelImage = req.body.hotelImage;
    var hotelCountry = req.body.hotelCountry;
    var hotelCity = req.body.hotelCity;
    var hotelAddress = req.body.hotelAddress;
    var hotelFacility = req.body.hotelFacility;
    var hotelPrice = req.body.hotelPrice;
    let sql = "INSERT INTO hotels.hoteldetails(user_id,name,images,price,country,city,address,facility,tag) VALUES ('" + user_id + "','" + hotelName + "','" + hotelImage + "','" + hotelPrice + "','" + hotelCountry + "','" + hotelCity + "','" + hotelAddress + "','" + hotelFacility + "','" + hotelTag + "')";
    let query = connection.query(sql, (err, results) => {
        if (err) throw err;
        console.log("1 record inserted");
        res.send(results);
    });
});

app.post('/userEmailPassword', (req, res) => {
    console.log(req.body);
    var Username = req.body.userName;
    var EmailId = req.body.emailId;
    var Password = req.body.password;
    let sql = "INSERT INTO hotels.users(user_name,email_id,password)VALUES ('" + Username + "','" + EmailId + "','" + Password + "')";
    let query = connection.query(sql, (err) => {
        if (err) throw err;
        console.log("Username & EmailId & Password inserted");
    });
});

app.get('/getUserInformtion', (req, res) => {
    let sql = "SELECT * FROM hotels.users";
    let query = connection.query(sql, (err, results) => {
        if (err) throw err;
        res.send(results);
    })
})

app.post('/updateUser', (req, res) => {
    console.log(req.body);
    let user_id = req.body.user_id;
    let user_name = req.body.user_name;
    let email_id = req.body.email_id;
    let password = req.body.password;
    let gender = req.body.gender;
    let birthday = req.body.birthday;
    let marital = req.body.marital;
    let mobile_number = req.body.mobile_number;
    let sql = "UPDATE `hotels`.`users` SET user_name = '" + user_name + "',email_id = '" + email_id + "',password = '" + password + "',gender = '" + gender + "', birth_date = '" + birthday + "',marital_status = '" + marital + "',  mobile_number = '" + mobile_number + "' WHERE id = '" + user_id + "';"
    let query = connection.query(sql, (err, results) => {
        if (err) throw err;
        console.log("User Profile updated successfully");
        res.send(results);
    })
});

app.get('/getAllProductDetails', (req, res) => {

    let sql = "SELECT * FROM hotels.hoteldetails";
    let query = connection.query(sql, (err, results) => {
        if (err) throw err;
        res.send(results);
    })
});


app.get('/getEditDataById', (req, res) => {
    hotelId = JSON.parse(req.query.hotelId);
    // hotelId.forEach(value => {
    let sql = "CALL getEditDataById('" + hotelId + "')";
    let query = connection.query(sql, (err, results) => {
        if (err) throw err;
        console.log("-------------------------------------------------");
        console.log("Record shown successfully");
        res.send(results);
        // });
    });
});

app.get('/getAllBuses', (req, res) => {
    let sql = "SELECT * FROM hotels.buses";
    let query = connection.query(sql, (err, results) => {
        if (err) throw err;
        console.log("-------------------------------------------------");
        console.log("Record shown successfully");
        res.send(results);
    })
});


app.post('/insertBusRecord', (req, res) => {
    console.log(req.body);
    let user_id = req.body.user_id;
    let busName = req.body.busName;
    let busImage = req.body.busImage;
    let busSeat = req.body.busSeat;
    let busStartingPoint = req.body.busStartingPoint;
    let busEndingPoint = req.body.busEndingPoint;
    let busRoute = req.body.busRoute;
    let fair = req.body.fair;
    let busTiming = req.body.busTiming;
    let busFacilities = req.body.busFacilities;
    let busType = req.body.busType;
    let sql = "INSERT INTO hotels.buses(user_id,name,images,bus_seat, bus_boarding_point,bus_dropping_point,bus_route,fair,bus_timing,facilites,bus_type)VALUES ('" + user_id + "','" + busName + "','" + busImage + "','" + busSeat + "','" + busStartingPoint + "','" + busEndingPoint + "','" + busRoute + "','" + fair + "','" + busTiming + "','" + busFacilities + "','" + busType + "')";
    let query = connection.query(sql, (err, results) => {
        if (err) throw err;
        console.log("-------------------------------------------------");
        console.log("1 Row inserted");
        res.send(results);
    })
});


app.get('/getAllBuses', (req, res) => {
    let sql = "SELECT * FROM hotels.buses";
    let query = connection.query(sql, (err, results) => {
        if (err) throw err;
        console.log("-------------------------------------------------");
        console.log("Record shown successfully");
        res.send(results);
    })
});
app.post('/editRecords', (req, res) => {
    console.log(req.body);
    let user_id = req.body.user_id;
    let id = req.body.id;
    let name = req.body.name;
    let images = req.body.images;
    let country = req.body.country;
    let city = req.body.city;
    let address = req.body.address;
    let facility = req.body.facility;
    let price = req.body.price;
    let sql = "UPDATE hotels.hoteldetails SET name = '" + name + "',images = '" + images + "',  price = '" + price + "',country = '" + country + "',city = '" + city + "',address = '" + address + "',facility = '" + facility + "' WHERE id = '" + id + "' AND user_id = '" + user_id + "'";
    let query = connection.query(sql, (err, results) => {
        if (err) throw err;
        console.log("Update records successfully");
        res.send(results);
    });
});

app.listen(1337);