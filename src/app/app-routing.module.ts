import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'hotels',
    pathMatch: 'full'
  },
  {
    path: 'hotels',
    loadChildren: () => import('./hotels/hotels.module').then( m => m.HotelsPageModule)
  },
  {
    path: 'sign-up-sign-in',
    loadChildren: () => import('./users/sign-up-sign-in/sign-up-sign-in.module').then( m => m.SignUpSignInPageModule)
  },
  {
    path: 'hotel-details/:id',
    loadChildren: () => import('./hotels/hotel-details/hotel-details.module').then( m => m.HotelDetailsPageModule)
  },
  {
    path: 'usersafety',
    loadChildren: () => import('./users/usersafety/usersafety.module').then( m => m.UsersafetyPageModule)
  },
  {
    path: 'add-item/:for',
    loadChildren: () => import('./add-edit/add-item/add-item.module').then( m => m.AddItemPageModule)
  },
  {
    path: 'edit-item/:for',
    loadChildren: () => import('./add-edit/edit-item/edit-item.module').then( m => m.EditItemPageModule)
  },
  {
    path: 'user-profile',
    loadChildren: () => import('./users/user-profile/user-profile.module').then( m => m.UserProfilePageModule)
  },
  {
    path: 'buses-cabs-trains',
    loadChildren: () => import('./buses-cabs-trains/buses-cabs-trains.module').then( m => m.BusesCabsTrainsPageModule)
  },
  {
    path: 'buses-cabs-trains-details/:id',
    loadChildren: () => import('./buses-cabs-trains/buses-cabs-trains-details/buses-cabs-trains-details.module').then( m => m.BusesCabsTrainsDetailsPageModule)
  },
  {
    path: 'shopping-cart',
    loadChildren: () => import('./shopping-cart/shopping-cart.module').then( m => m.ShoppingCartPageModule)
  },
  {
    path: 'calendar',
    loadChildren: () => import('./hotels/calendar/calendar.module').then( m => m.CalendarPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
