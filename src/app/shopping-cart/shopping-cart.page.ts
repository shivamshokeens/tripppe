import { Component, OnInit, Injectable, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { hotelService } from '../hotels/hotel.service';
import { userService } from '../users/user.service';
import { ModalController, AlertController } from '@ionic/angular';
import { shoppingService } from './shopping.service';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.page.html',
  styleUrls: ['./shopping-cart.page.scss'],
})
export class ShoppingCartPage implements OnInit {
  searchTerm;

  items: any;

  constructor(public route: ActivatedRoute, public router: Router, public hotel: hotelService, public Userservice: userService, public modalController: ModalController, public Shoppingservice: shoppingService, public alertController: AlertController,) { }

  ngOnInit() {
    if (this.Userservice.loggedInUser) {
      //this.setFilteredItems();
    }
    else {
      this.router.navigate(['/hotels']);
    }
  }

  setFilteredItems() {
    // console.log(this.searchTerm)
    // this.items = this.Shoppingservice.filterItems(this.searchTerm);
    // console.log(this.items);
    this.Workinprogress();
  }

  async Workinprogress() {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: 'Work in progress',
      buttons: ['OK']
    });

    await alert.present();
  }

}
