import { Injectable } from '@angular/core';
import { hotelService } from '../hotels/hotel.service';
import { userService } from '../users/user.service';
import { ModalController } from '@ionic/angular';

@Injectable({ providedIn: 'root' })
export class shoppingService {

    hotelDetails =
        [
            {
                id: "",
                name: "",
                price: 0,
                images: ""
            }
        ];
    // hotelDetails = []
    priceList = [];
    grandTotal;

    arrSlice = [];
    constructor(public hotel: hotelService, public Userservice: userService, public modalController: ModalController,) { }

    shoppingList(id, name, price, images) {
        if (this.Userservice.loggedInUser) {
            console.log(id + name + price + images)
            this.hotelDetails.push({
                id: id,
                name: name,
                price: parseInt(price),
                images: images
            });
            console.log(this.hotelDetails);
            this.priceCalculation();
        }
    }

    filterItems(searchTerm) {
        if (this.hotelDetails != undefined) {
            console.log(searchTerm)
            console.log(this.hotelDetails);
            return this.hotelDetails.filter(item => {
                return item.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
            });
        }
    }

    priceCalculation() {
        this.arrSlice = this.hotelDetails.slice(-1);
        this.arrSlice.forEach((value) => {
            if (value.price != undefined || value.price != '') {
                this.priceList.push(value.price);
            }
        });
        console.log(this.priceList);
        this.grandTotal = this.priceList.map(a => a).reduce(function (a, b) {
            return a + b;
        });
        console.log(this.grandTotal);
    }

}