import { Component, OnInit, Input } from '@angular/core';
import { userService } from 'src/app/users/user.service';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.page.html',
  styleUrls: ['./edit-item.page.scss'],
})

export class EditItemPage implements OnInit {

  editProduct: FormGroup;
  @Input('hotelId') hotelId;
  @Input('countries') countries;
  ProductDetails;
  editProduct2: FormGroup;

  constructor(public Userservice: userService, public http: HttpClient, private router: Router,public modalController: ModalController) { }

  ngOnInit() {
    this.http.get("http://localhost:1337/getEditDataById", {
      params: {
        hotelId: JSON.stringify(this.hotelId)
      }
    }).subscribe(responseData => {
      this.ProductDetails = responseData[0];
      console.log(this.ProductDetails);
      console.log(this.ProductDetails[0]);
      this.edit();
    });
  }

  public edit() {
    let user_id = 0;
    let id = 0;
    let name = "";
    let images = "";
    let price = "";
    let country = "";
    let city = "";
    let address = "";
    let facility = "";
    let tag = "";

    this.ProductDetails.forEach(value => {
      console.log(this.ProductDetails);
      user_id = value.user_id;
      id = value.id;
      name = value.name;
      images = value.images;
      price = value.price;
      country = value.country;
      city = value.city;
      address = value.address;
      facility = value.facility;
      tag = value.tag;
    });

    this.editProduct = new FormGroup({
      'user_id': new FormControl(user_id),
      'id': new FormControl(id),
      'name': new FormControl(name),
      'images': new FormControl(images),
      'price': new FormControl(price),
      'country': new FormControl(country),
      'city': new FormControl(city),
      'address': new FormControl(address),
      'facility': new FormControl(facility),
      'tag': new FormControl(tag)
    });

  }
  submit(Form: NgForm) {
    if (Form.valid) {
      console.log(Form.value);
      this.http.post('http://localhost:1337/editRecords', Form.value).subscribe(responseData =>{
        console.log("Send successfully");
        this.modalController.dismiss();
        this.router.navigate(['/hotels']);
      },error =>{
        console.log(error);
      })
    }
  }

  onDismiss() {
    this.modalController.dismiss();
  }

}
