import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { userService } from 'src/app/users/user.service';
import { ToastController, ModalController } from '@ionic/angular';
import { EditItemPageModule } from '../edit-item/edit-item.module';
import { EditItemPage } from '../edit-item/edit-item.page';
import { hotelService } from 'src/app/hotels/hotel.service';


@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.page.html',
  styleUrls: ['./add-item.page.scss'],
})

export class AddItemPage implements OnInit {

  type;
  productType;
  Type;

  hotelDetails = {
    user_id: 0,
    hotelName: "",
    hotelImage: "",
    hotelCountry: "",
    hotelCity: "",
    hotelAddress: "",
    hotelFacility: "",
    hotelTag: "",
    hotelPrice: ""
  }

  BusDetails = {
    user_id: 0,
    busName: "",
    busImage: "",
    busSeat: "",
    busStartingPoint: "",
    busEndingPoint: "",
    busRoute: "",
    fair: "",
    busTiming: "",
    busFacilities: "",
    busType: "",
    ratings: ""
  }

  countries;

  userEditProducts: any;
  tagSelected;

  constructor(private toastController: ToastController, private router: Router, private route: ActivatedRoute, public http: HttpClient, public Userservice: userService, public modalController: ModalController, public hotel: hotelService) {
    this.type = this.route.snapshot.params['for'];
    console.log(this.type);
  }

  ngOnInit() {
    if (this.Userservice.loggedInUser) {
      this.hotelDetails.user_id = this.Userservice.loggedInUserId;
      this.BusDetails.user_id = this.Userservice.loggedInUserId;
      this.http.get('http://localhost:1337/getCountry').subscribe(responseData => {
        this.countries = responseData;
      });
      if (this.type == 'edit' && this.Userservice.productDetails) {
        console.log(this.Userservice.productDetails);
        this.userEditProducts = this.Userservice.productDetails.filter((value) => this.Userservice.loggedInUserId == value.user_id);
        console.log(this.userEditProducts);
      }
    }
    else {
      this.router.navigate(['/hotels']);
    }
  }


  Tagselected(value) {
    this.tagSelected = value;
    console.log(value);
  }

  setFilteredItems() {
    this.productType = this.Type;
    if (this.Type != 'Buses') {
      this.hotelDetails.hotelTag = this.Type;
      console.log(this.hotelDetails.hotelTag)
    }
    console.log("Selected option value " + this.Type);
  }

  busForm(Form) {
    console.log("Selected option value " + this.productType);
    if (Form.valid) {
      console.log(Form);
      console.log(this.BusDetails);
      this.http.post('http://localhost:1337/insertBusRecord', this.BusDetails).subscribe(responseData => {
        console.log("Request sent to insertBusRecord");
        this.router.navigate(['/hotels']);
      })
    }
  }


  hotelSubmit(Form: NgForm) {
    if (Form.valid == true) {
      console.log(this.hotelDetails);
      this.http.post("http://localhost:1337/addProduct", this.hotelDetails).subscribe(responseData => {
        console.log(responseData);
        this.router.navigate(['/hotels']);
        this.SuccessfullMessage();
      });
    }
  }

  async SuccessfullMessage() {
    const toast = await this.toastController.create({
      message: 'Added Successfully.If not refresh hotel page',
      position: 'bottom',
      duration: 4500
    });
    toast.present();
  }

  async moveToEditPage(id) {
    const modal = await this.modalController.create({
      component: EditItemPage,
      componentProps: { hotelId: id, countries: this.countries }
    })
    return await modal.present();
  }

  onDismiss() {
    this.modalController.dismiss();
  }


}
