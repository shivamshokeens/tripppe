import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HomePageModule } from './home/home.module';
import { SignUpSignInPageModule } from './users/sign-up-sign-in/sign-up-sign-in.module';
import { hotelService } from './hotels/hotel.service';
import { HotelDetailsPageModule } from './hotels/hotel-details/hotel-details.module';
import { UsersafetyPageModule } from './users/usersafety/usersafety.module';


import { HttpClientModule } from '@angular/common/http';
import { EditItemPageModule } from './add-edit/edit-item/edit-item.module';
import { AddItemPageModule } from './add-edit/add-item/add-item.module';
import { ReactiveFormsModule } from '@angular/forms';
import { userService } from './users/user.service';
import { BusesCabsTrainsPageModule } from './buses-cabs-trains/buses-cabs-trains.module';
import { BusesCabsTrainsDetailsPageModule } from './buses-cabs-trains/buses-cabs-trains-details/buses-cabs-trains-details.module';
import { busesService } from './buses-cabs-trains/busesService.service';
import { UserProfilePageModule } from './users/user-profile/user-profile.module';
import { shoppingService } from './shopping-cart/shopping.service';
import { ShoppingCartPageModule } from './shopping-cart/shopping-cart.module';
import { CalendarPageModule } from './hotels/calendar/calendar.module';
import { calendarService } from './hotels/calendar/calendar.service';
import { CalendarModule } from 'ion2-calendar';

@NgModule({
  declarations: [
    AppComponent,
    
  ],
  entryComponents:
    [

    ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    CalendarPageModule,
    HomePageModule,
    SignUpSignInPageModule,
    HotelDetailsPageModule,
    UsersafetyPageModule,
    HttpClientModule,
    AddItemPageModule,
    EditItemPageModule,
    ReactiveFormsModule,
    BusesCabsTrainsPageModule,
    BusesCabsTrainsDetailsPageModule,
    UserProfilePageModule,
    ShoppingCartPageModule,
    CalendarModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    hotelService,
    userService,
    busesService,
    shoppingService,
    calendarService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
