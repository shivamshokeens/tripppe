import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { SignUpSignInPage } from '../users/sign-up-sign-in/sign-up-sign-in.page';
import { hotelService } from './hotel.service';
import { HttpClient } from '@angular/common/http';
import { userService } from '../users/user.service';

@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.page.html',
  styleUrls: ['./hotels.page.scss'],
})
export class HotelsPage implements OnInit {

  public searchTerm: string = "";
  public items: any;

  Displayhotel = true;
  Displayvillas = false;
  constructor(public modalController: ModalController, public hotel: hotelService, public http: HttpClient, public alertController: AlertController,public Userservice:userService) {
    this.Displayhotel = true;
  }
  ngOnInit() {
    if (this.Displayhotel = true) {
      this.http.get('http://localhost:1337/getHotel').subscribe(responseData => {
        this.hotel.hotelDetails = responseData;
        this.setFilteredItems();
      });
    }
  }

  setFilteredItems() {
    this.items = this.hotel.filterItems(this.searchTerm);
  }

  hotels() {
    this.Displayhotel = true;
    this.Displayvillas = false;
    this.http.get('http://localhost:1337/getHotel').subscribe(responseData => {
      this.hotel.hotelDetails = responseData;
      this.setFilteredItems();
    });
  }

  villas() {
    this.Displayvillas = true;
    this.Displayhotel = false;
    this.http.get('http://localhost:1337/getVillas&Appartment').subscribe(responseData => {
      this.hotel.hotelDetails = responseData;
      this.setFilteredItems();
    });
  }

  async flights() {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: 'Due to COVID-19 no flights are available',
      buttons: ['OK']
    });

    await alert.present();
  }

  async Workinprogress() {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: 'Work in progress',
      buttons: ['OK']
    });

    await alert.present();
  }

  async SignupSignin() {
    const modal = await this.modalController.create({
      component: SignUpSignInPage,
      cssClass: 'borderRadius',
    });
    return await modal.present();
  }

}
