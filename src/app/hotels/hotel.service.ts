import { Injectable } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { userService } from '../users/user.service';

@Injectable({ providedIn: 'root' })
export class hotelService {

    hotelDetails;

    constructor(public modalController: ModalController, public http: HttpClient, public alertController: AlertController, public Userservice: userService) {
    }

    filterItems(searchTerm) {
        if (this.hotelDetails != undefined) {
            return this.hotelDetails.filter(item => {
                return item.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 || item.city.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 || item.country.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 || item.address.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
            });
        }
    }
}