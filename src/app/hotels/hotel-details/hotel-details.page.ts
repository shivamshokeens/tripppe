import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { hotelService } from '../hotel.service';
import { userService } from 'src/app/users/user.service';
import { ModalController } from '@ionic/angular';
import { CalendarPage } from '../calendar/calendar.page';
import { ShoppingCartPage } from 'src/app/shopping-cart/shopping-cart.page';
import { shoppingService } from 'src/app/shopping-cart/shopping.service';

@Component({
  selector: 'app-hotel-details',
  templateUrl: './hotel-details.page.html',
  styleUrls: ['./hotel-details.page.scss'],
})
export class HotelDetailsPage implements OnInit {

  hotelDetails;

  constructor(public route : ActivatedRoute,public router : Router,public hotel : hotelService,public Userservice : userService,public modalController: ModalController,private Shoppingservice: shoppingService) { }

  ngOnInit() {
    console.log(this.route.snapshot.params['id']);
    if(this.Userservice.loggedInUser){
    this.allFilters();
    }
  }

  allFilters(){
    this.hotelDetails = this.hotel.hotelDetails.filter((value)=>value.id == this.route.snapshot.params['id']);
  }

  async openCalendar() {
    const modal = await this.modalController.create({
      component: ShoppingCartPage
    });
    return await modal.present();
  }

  moveToShoppingCart(id,name,price,images){
    this.Shoppingservice.shoppingList(id,name,price,images);
    this.router.navigate(['/shopping-cart']);
  }
}
