import { Component } from '@angular/core';

import { Platform, ModalController, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { hotelService } from './hotels/hotel.service';
import { HttpClient } from '@angular/common/http';
import { SignUpSignInPage } from './users/sign-up-sign-in/sign-up-sign-in.page';
import { userService } from './users/user.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public modalController: ModalController,
    public hotel: hotelService,
    public http: HttpClient,
    public alertController: AlertController,
    public Userservice : userService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  async Workinprogress() {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: 'Work in progress',
      buttons: ['OK']
    });

    await alert.present();
  }

  async SignupSignin() {
    const modal = await this.modalController.create({
      component: SignUpSignInPage,
      cssClass: 'borderRadius',
    });
    return await modal.present();
  }
}
