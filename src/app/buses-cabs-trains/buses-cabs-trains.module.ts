import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BusesCabsTrainsPageRoutingModule } from './buses-cabs-trains-routing.module';

import { BusesCabsTrainsPage } from './buses-cabs-trains.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BusesCabsTrainsPageRoutingModule
  ],
  declarations: [BusesCabsTrainsPage]
})
export class BusesCabsTrainsPageModule {}
