import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusesCabsTrainsPage } from './buses-cabs-trains.page';

const routes: Routes = [
  {
    path: '',
    component: BusesCabsTrainsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusesCabsTrainsPageRoutingModule {}
