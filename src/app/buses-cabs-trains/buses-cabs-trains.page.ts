import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { hotelService } from '../hotels/hotel.service';
import { userService } from '../users/user.service';
import { busesService } from './busesService.service';

@Component({
  selector: 'app-buses-cabs-trains',
  templateUrl: './buses-cabs-trains.page.html',
  styleUrls: ['./buses-cabs-trains.page.scss'],
})
export class BusesCabsTrainsPage implements OnInit {

  Busesdetails;
  boardingRoute: string = "";
  destinationRoute: string = "";
  public items: any;

  constructor(public modalController: ModalController, public hotel: hotelService, public http: HttpClient, public alertController: AlertController, public Userservice: userService, public Busservice: busesService) { }

  ngOnInit() {
    this.http.get('http://localhost:1337/getAllBuses').subscribe(responseData => {
      this.Busservice.Details = responseData;
      this.setFilteredItems();
    });
  }

  setFilteredItems() {
    this.items = this.Busservice.filterItems(this.boardingRoute,this.destinationRoute);
  }

  async Workinprogress() {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: 'Work in progress',
      buttons: ['OK']
    });

    await alert.present();
  }

}
