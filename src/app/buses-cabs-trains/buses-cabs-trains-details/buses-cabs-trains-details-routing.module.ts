import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BusesCabsTrainsDetailsPage } from './buses-cabs-trains-details.page';

const routes: Routes = [
  {
    path: '',
    component: BusesCabsTrainsDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BusesCabsTrainsDetailsPageRoutingModule {}
