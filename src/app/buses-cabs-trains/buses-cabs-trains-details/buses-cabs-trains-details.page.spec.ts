import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BusesCabsTrainsDetailsPage } from './buses-cabs-trains-details.page';

describe('BusesCabsTrainsDetailsPage', () => {
  let component: BusesCabsTrainsDetailsPage;
  let fixture: ComponentFixture<BusesCabsTrainsDetailsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusesCabsTrainsDetailsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BusesCabsTrainsDetailsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
