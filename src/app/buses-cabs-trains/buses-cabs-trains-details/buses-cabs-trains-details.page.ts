import { Component, OnInit } from '@angular/core';
import { busesService } from '../busesService.service';
import { ActivatedRoute, Router } from '@angular/router';
import { hotelService } from 'src/app/hotels/hotel.service';
import { userService } from 'src/app/users/user.service';

@Component({
  selector: 'app-buses-cabs-trains-details',
  templateUrl: './buses-cabs-trains-details.page.html',
  styleUrls: ['./buses-cabs-trains-details.page.scss'],
})
export class BusesCabsTrainsDetailsPage implements OnInit {

  busDetails;
  busId = 0;
  buttonsTexts:Array<string> = ['First button'];
  
  constructor(public Busesservice : busesService,private route:ActivatedRoute,private router:Router,public hotel : hotelService,public Userservice : userService ) { }

  ngOnInit() {    
    this.busId = this.route.snapshot.params['id'];
    if(this.Busesservice.Details){
    this.busDetails = this.Busesservice.Details.filter((value) => this.busId == value.id );
    console.log(this.busDetails)
    }
    else{
      this.router.navigate(['/buses-cabs-trains']);
    }
  }
  seatSelected(index:number):void {

    for(index= 1 ; index < 10; index ++){
    console.log(index)
    this.buttonsTexts = [...this.buttonsTexts, `A ${index}`];
    }
    // index  ++;
    
  }

  selectedSeat(value){
    console.log(value);
  }

}
