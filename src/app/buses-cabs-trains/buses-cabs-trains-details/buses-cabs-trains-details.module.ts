import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BusesCabsTrainsDetailsPageRoutingModule } from './buses-cabs-trains-details-routing.module';

import { BusesCabsTrainsDetailsPage } from './buses-cabs-trains-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BusesCabsTrainsDetailsPageRoutingModule
  ],
  declarations: [BusesCabsTrainsDetailsPage]
})
export class BusesCabsTrainsDetailsPageModule {}
