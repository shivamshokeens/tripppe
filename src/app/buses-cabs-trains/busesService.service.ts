import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class busesService {

    Details;
    filterItems(boardingRoute, destinationRoute) {
        if (this.Details != undefined) {
            return this.Details.filter(item => {
                return item.name.toLowerCase().indexOf(boardingRoute.toLowerCase()) > -1 || item.bus_boarding_point.toLowerCase().indexOf(boardingRoute.toLowerCase()) > -1 &&
                    item.name.toLowerCase().indexOf(destinationRoute.toLowerCase()) > -1 || item.bus_dropping_point.toLowerCase().indexOf(destinationRoute.toLowerCase()) > -1;
            });
        }
    }
}