import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BusesCabsTrainsPage } from './buses-cabs-trains.page';

describe('BusesCabsTrainsPage', () => {
  let component: BusesCabsTrainsPage;
  let fixture: ComponentFixture<BusesCabsTrainsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BusesCabsTrainsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BusesCabsTrainsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
