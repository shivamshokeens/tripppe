import { Injectable } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalController, ToastController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { AddItemPageModule } from '../add-edit/add-item/add-item.module';
import { HttpParams } from "@angular/common/http";
import { JsonPipe } from '@angular/common';

@Injectable({
    providedIn: "root"
})
export class userService {

    usersigninDetails;
    loggedInUser;
    loggedInUserId;
    jsonUserId = {
        user_id: ""
    }
    productDetails;

    constructor(private toastController: ToastController, private router: Router, private route: ActivatedRoute, private modalController: ModalController, private http: HttpClient,) {
        this.http.get("http://localhost:1337/getUserInformtion").subscribe(responseData => {
            this.usersigninDetails = responseData;
        });

    }

    emailVerification(User) {
        let verifyDuplicateEmailid = this.usersigninDetails.filter((value) => value.email_id === User.emailId);
        if (verifyDuplicateEmailid.length > 1) {
            this.EmailMessage();
        }
        else {
            this.http.post("http://localhost:1337/userEmailPassword", User).subscribe(ResponseData => {
                console.log("Successfully");
            });
        }

    }

    loginAuth(Email, Password) {
        let verify = this.usersigninDetails.filter((value) => value.email_id === Email && value.password === Password);
        console.log(verify);
        if (verify.length > 0) {
            this.loggedInUser = verify;
            verify.forEach(value => {
                this.loggedInUserId = value.id;
                this.jsonUserId.user_id = value.id;
            });
            if (this.loggedInUser) {
                this.http.get('http://localhost:1337/getAllProductDetails').subscribe(responseData => {
                    this.productDetails = responseData;
                    console.log("Request sent successfully of getAllProductDetails")
                });
            }
            this.SignUpMessage();
            this.router.navigate(['/hotels']);
            this.modalController.dismiss();
        }
        else {
            this.SignUpErrorMessage();
        }
    }

    async SignUpErrorMessage() {
        const toast = await this.toastController.create({
            message: 'Either Email id or password is wrong.',
            position: 'bottom',
            duration: 2500
        });
        toast.present();
    }

    async SignUpMessage() {
        const toast = await this.toastController.create({
            message: 'Signup successfully.',
            position: 'bottom',
            duration: 2500
        });
        toast.present();
    }

    async EmailMessage() {
        const toast = await this.toastController.create({
            message: 'Email id already exist.',
            position: 'bottom',
            duration: 2500
        });
        toast.present();
    }
}