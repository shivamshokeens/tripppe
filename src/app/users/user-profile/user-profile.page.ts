import { Component, OnInit } from '@angular/core';
import { userService } from '../user.service';
import { ModalController, AlertController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.page.html',
  styleUrls: ['./user-profile.page.scss'],
})
export class UserProfilePage implements OnInit {

  editProfile: FormGroup;
  categories = [
    {
      name: "Male"
    },
    {
      name: "Female"
    },
    {
      name: "Other"
    },
  ];
  MaritalStatus = [
    {
      name: "Single"
    },
    {
      name: "Married"
    }
  ];

  constructor(private router: Router, private route: ActivatedRoute, public Userservice: userService, public modalController: ModalController, public http: HttpClient, public alertController: AlertController) {
  }

  ngOnInit() {
    if (this.Userservice.loggedInUser) {
      this.userEditForm();
    }
    else {
      this.router.navigate(['/hotels']);
    }
  }

  userEditForm() {
    let user_id = 0;
    let user_name = '';
    let email_id = '';
    let password = '';
    let gender = '';
    let birthday = '';
    let marital = '';
    let mobile_number = '';

    this.Userservice.loggedInUser.forEach((value) => {
      user_id = value.id;
      user_name = value.user_name;
      email_id = value.email_id;
      password = value.password;
      gender = value.gender;
      birthday = value.birth_date;
      marital = value.marital_status;
      mobile_number = value.mobile_number;
    });

    this.editProfile = new FormGroup({
      'user_id': new FormControl(user_id),
      'user_name': new FormControl(user_name),
      'email_id': new FormControl(email_id),
      'password': new FormControl(password),
      'gender': new FormControl(gender), 
      'birthday': new FormControl(birthday),
      'marital': new FormControl(marital),
      'mobile_number': new FormControl(mobile_number)
    });
  }

  save(Form: NgForm) {
    console.log(Form);
    if (Form.valid) {
      this.http.post('http://localhost:1337/updateUser', Form.value).subscribe(responseData => {
        this.router.navigate(['/hotels']);
        console.log("Successfully send");
      })
    }
  }

}
