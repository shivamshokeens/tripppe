import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ToastController, ModalController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { userService } from '../user.service';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-sign-up-sign-in',
  templateUrl: './sign-up-sign-in.page.html',
  styleUrls: ['./sign-up-sign-in.page.scss'],
})
export class SignUpSignInPage implements OnInit {

  User = {
    userName: "",
    emailId: "",
    password: ""
  }

  Userlogin = {
    emailId: "",
    password: ""
  }

  Login = true;
  constructor(public toastController: ToastController, public router: Router, public route: ActivatedRoute, public modalController: ModalController, public Userservice: userService, private http: HttpClient) { }

  ngOnInit() {
  }

  submit() {
    this.Login = false;
  }
  signin(Form: NgForm) {
    if (Form.valid) {
      this.Userservice.emailVerification(this.User);
      this.Login = true;
    }
  }

  signup(loginForm: NgForm) {
    if (loginForm.valid) {
      this.Userservice.loginAuth(loginForm.value.emailId, loginForm.value.password)
    }
  }

  onDismiss() {
    this.modalController.dismiss();
  }

}
