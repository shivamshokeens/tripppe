import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SignUpSignInPage } from './sign-up-sign-in.page';

const routes: Routes = [
  {
    path: '',
    component: SignUpSignInPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SignUpSignInPageRoutingModule {}
