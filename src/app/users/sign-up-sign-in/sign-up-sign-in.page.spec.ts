import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SignUpSignInPage } from './sign-up-sign-in.page';

describe('SignUpSignInPage', () => {
  let component: SignUpSignInPage;
  let fixture: ComponentFixture<SignUpSignInPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignUpSignInPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SignUpSignInPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
