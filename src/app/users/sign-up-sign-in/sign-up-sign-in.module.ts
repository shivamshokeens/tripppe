import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SignUpSignInPageRoutingModule } from './sign-up-sign-in-routing.module';

import { SignUpSignInPage } from './sign-up-sign-in.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SignUpSignInPageRoutingModule
  ],
  declarations: [SignUpSignInPage]
})
export class SignUpSignInPageModule {}
