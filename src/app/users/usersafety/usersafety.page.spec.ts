import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UsersafetyPage } from './usersafety.page';

describe('UsersafetyPage', () => {
  let component: UsersafetyPage;
  let fixture: ComponentFixture<UsersafetyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersafetyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UsersafetyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
