import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersafetyPage } from './usersafety.page';

const routes: Routes = [
  {
    path: '',
    component: UsersafetyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersafetyPageRoutingModule {}
