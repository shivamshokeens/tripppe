import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UsersafetyPageRoutingModule } from './usersafety-routing.module';

import { UsersafetyPage } from './usersafety.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UsersafetyPageRoutingModule
  ],
  declarations: [UsersafetyPage]
})
export class UsersafetyPageModule {}
