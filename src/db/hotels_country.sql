-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: hotels
-- ------------------------------------------------------
-- Server version	5.6.47-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'India'),(2,'Afghanistan'),(3,'Albania'),(4,'Algeria'),(5,'Andorra'),(6,'Angola'),(7,'Antigua and Barbuda'),(8,'Argentina'),(9,'Armenia'),(10,'Australia'),(11,'Austria'),(12,'Austrian Empire'),(13,'Azerbaijan'),(14,'Baden'),(15,'Bahamas'),(16,'Bahrain'),(17,'Bangladesh'),(18,'Barbados'),(19,'Bavaria'),(20,'Belarus'),(21,'Belgium'),(22,'Belize'),(23,'Benin'),(24,'Bolivia'),(25,'Bosnia and Herzegovina'),(26,'Botswana'),(27,'Brazil'),(28,'Brunei Darussalam'),(29,'Bulgaria'),(30,'Burkina Faso'),(31,'Burundi'),(32,'Cabo Verde'),(33,'Cambodia'),(34,'Cameroon'),(35,'Canada'),(36,'Central African Republic'),(37,'Chad'),(38,'Chile'),(39,'China'),(40,'Colombia'),(41,'Comoros'),(42,'Congo'),(43,'Cook Islands'),(44,'Costa Rica'),(45,'Croatia'),(46,'Cuba'),(47,'Cyprus'),(48,'Czechia'),(49,'Côte d\'Ivoire'),(50,'Democratic People\'s Republic of Korea'),(51,'Democratic Republic of the Congo'),(52,'Denmark'),(53,'Djibouti'),(54,'Dominica'),(55,'Dominican Republic'),(56,'Ecuador'),(57,'Egypt'),(58,'El Salvador'),(59,'Equatorial Guinea'),(60,'Eritrea'),(61,'Estonia	'),(62,'Eswatini'),(63,'Ethiopia'),(64,'Faroe Islands'),(65,'Fiji'),(66,'Finland	'),(67,'France'),(68,'Gabon'),(69,'Gambia'),(70,'Georgia'),(71,'Germany'),(72,'Ghana'),(73,'Greece'),(74,'Grenada	'),(75,'Guatemala'),(76,'Guinea'),(77,'Guinea-Bissau'),(78,'Guyana'),(79,'Haiti'),(80,'Honduras'),(81,'Hungary'),(82,'Iceland	'),(83,'Indonesia'),(84,'Iran (Islamic Republic of)'),(85,'Iraq'),(86,'Ireland'),(87,'Israel'),(88,'Italy'),(89,'Jamaica'),(90,'Japan'),(91,'Jordan'),(92,'Kazakhstan'),(93,'Kenya'),(94,'Kiribati'),(95,'Kuwait'),(96,'Kyrgyzstan'),(97,'Lao People\'s Democratic Republic'),(98,'Latvia'),(99,'Lebanon	'),(100,'Lesotho'),(101,'Liberia'),(102,'Libya'),(103,'Lithuania'),(104,'Luxembourg'),(105,'Madagascar'),(106,'Malawi'),(107,'Malaysia'),(108,'Maldives'),(109,'Mali'),(110,'Malta'),(111,'Marshall Islands'),(112,'Mauritania'),(113,'Mauritius'),(114,'Mexico'),(115,'Micronesia (Federated States of)'),(116,'Monaco'),(117,'Mongolia'),(118,'Montenegro'),(119,'Morocco'),(120,'Mozambique'),(121,'Myanmar'),(122,'Namibia'),(123,'Nauru'),(124,'Nepal'),(125,'Netherlands'),(126,'New Zealand'),(127,'Nicaragua'),(128,'Niger'),(129,'Nigeria	'),(130,'Niue'),(131,'North Macedonia'),(132,'Norway'),(133,'Oman'),(134,'Pakistan'),(135,'Palau'),(136,'Panama'),(137,'Papua New Guinea'),(138,'Paraguay'),(139,'Peru'),(140,'Philippines'),(141,'Poland	'),(142,'Portugal'),(143,'Qatar'),(144,'Republic of Korea'),(145,'Republic of Moldova'),(146,'Romania'),(147,'Russian Federation'),(148,'Rwanda'),(149,'Saint Kitts and Nevis'),(150,'Saint Lucia	'),(151,'Saint Vincent and the Grenadines'),(152,'Samoa'),(153,'San Marino'),(154,'Sao Tome and Principe'),(155,'Saudi Arabia'),(156,'Senegal'),(157,'Serbia'),(158,'Seychelles'),(159,'Sierra Leone'),(160,'Singapore'),(161,'Slovakia'),(162,'Slovenia'),(163,'Solomon Islands'),(164,'Somalia'),(165,'South Africa'),(166,'South Sudan'),(167,'Spain'),(168,'Sri Lanka'),(169,'Sudan'),(170,'Suriname'),(171,'Sweden'),(172,'Switzerland'),(173,'Syrian Arab Republic'),(174,'Tajikistan'),(175,'Thailand'),(176,'Timor-Leste'),(177,'Togo'),(178,'Tokelau'),(179,'Tonga'),(180,'Trinidad and Tobago'),(181,'Tunisia	'),(182,'Turkey'),(183,'Turkmenistan'),(184,'Tuvalu'),(185,'Uganda'),(186,'Ukraine'),(187,'United Arab Emirates'),(188,'United Kingdom of Great Britain and Northern Ireland'),(189,'United Republic of Tanzania'),(190,'United States of America'),(191,'Uruguay'),(192,'Uzbekistan'),(193,'Vanuatu'),(194,'Venezuela (Bolivarian Republic of)'),(195,'Viet Nam'),(196,'Yemen	'),(197,'Zambia'),(198,'Zimbabwe');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-26 16:18:52
