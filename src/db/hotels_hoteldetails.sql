-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: hotels
-- ------------------------------------------------------
-- Server version	5.6.47-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hoteldetails`
--

DROP TABLE IF EXISTS `hoteldetails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hoteldetails` (
  `user_id` varchar(45) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `images` varchar(250) NOT NULL,
  `price` varchar(45) NOT NULL,
  `country` varchar(45) NOT NULL,
  `city` varchar(45) NOT NULL,
  `address` varchar(45) NOT NULL,
  `facility` varchar(45) NOT NULL,
  `tag` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hoteldetails`
--

LOCK TABLES `hoteldetails` WRITE;
/*!40000 ALTER TABLE `hoteldetails` DISABLE KEYS */;
INSERT INTO `hoteldetails` VALUES ('1',1,'Resort & spa','https://cdn.pixabay.com/photo/2012/11/21/10/24/building-66789__340.jpg','2000','India','New Delhi','Delhi/NCR','Free Wifi,Free Parking,Swimming Pool','hotels'),('2',2,'Tests','https://cdn.pixabay.com/photo/2020/06/12/06/58/silhouettes-5289412__340.jpg','1000','India','Goa','GOA','Free Breakfast,Dinner','hotels'),('3',3,'Ocean','https://media.istockphoto.com/photos/maid-working-at-a-hotel-and-doing-the-bed-wearing-a-facemask-picture-id1225040484?b=1&k=6&m=1225040484&s=170667a&w=0&h=6EIWJh0-lcIyW0TTzh9HDdSFYD7v3Is-XB1LhvXdRjM=','1500','Goa,India','Calangute','GOA','Free Wifi,Doctor on call,Swimming Pool','hotels'),('1',6,'Residency Miramar','https://cdn.pixabay.com/photo/2016/03/28/09/34/bedroom-1285156_960_720.jpg','1800','Goa,India','Panjim','Panjim','Free Wifi,Doctor on call,Swimming Pool','hotels'),('2',7,'Palms','https://media.istockphoto.com/photos/airy-bedroom-interior-with-beautiful-view-picture-id1142850528?b=1&k=6&m=1142850528&s=170667a&w=0&h=4zJPkNWawVcmjJ412JHkPP2oEgqfvEFk3zVTlp8_iic=','2000','India','rimjim','rimjim','Free Wifi,Doctor on call,Swimming Pool','hotels'),('3',8,'Blue Hermitage','https://cdn.pixabay.com/photo/2017/06/12/15/08/canal-2395818__340.jpg','4000','Goa','kimjim','kimjim','Free Parking,Free drinks','villas'),('1',9,'Green Hermitage','https://cdn.pixabay.com/photo/2018/09/29/15/07/hermitage-3711588__340.jpg','5000','Goa','rim','rim','Free games','villas'),('2',10,'Lemon Appartment','https://cdn.pixabay.com/photo/2016/10/21/16/57/russia-1758515__340.jpg','4500','Goa','rim','rim','Free nothing','Appartment'),('3',11,'Rose Appartment','https://cdn.pixabay.com/photo/2019/03/25/18/23/santa-caterina-del-sasso-4080976__340.jpg','6000','Goa','rim','rim','Free nothing','Appartment'),('1',12,'Dummy','https://cdn.pixabay.com/photo/2020/05/20/03/50/gears-5193383__340.png','10000',' India ','Dummy','Dummy','Dummy','villas'),('2',13,'KK ','https://cdn.pixabay.com/photo/2020/06/14/15/55/landscape-5298395__340.jpg','100',' India ','New Delhi','New delhi near GH-14','Free parking','hotels'),('2',14,'KK','https://cdn.pixabay.com/photo/2020/06/14/14/44/drink-5298126__340.jpg','500',' India ','New Delhi','New delhi near GH-14','Free parking','hotels'),('2',15,'zxxz','https://cdn.pixabay.com/photo/2020/05/27/11/01/away-5226757__340.jpg','2000',' India ','New delhi','Peeragarhi Village ,New Delhi','Free parking','villas');
/*!40000 ALTER TABLE `hoteldetails` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-26 16:18:53
